from os import chdir, getcwd, remove, scandir
from os.path import exists
from datetime import datetime
from tkinter import filedialog
from shutil import copyfile
from validators import url as is_url_valid

SUPPORTED_EXT_LIST = ["mp4", "avi", "jpg", "png", "jpeg"]
PLAYER_PATH = "/home/pi/Videos/viewer_content/"
SMB_PATH = "/mnt/bczdat01_kmtv/"
DEFAULT_IMG_PATH = "default.png"
LINK_FILE = "/home/pi/Videos/viewer_content/link.txt"


def get_tv_data() -> list[list[str]]:
    # TODO: connect DB and for current user FETCH tv data
    return [
        ["TV1", "192.168.50.48", "PHA-recepce", "reklamni", "UNKNOWN"],
        ["TV2", "192.168.1.34", "OVA-recepce", "reklamni", "UNKNOWN"],
        ["TV3", "192.168.50.2", "BRN-recepce", "reklamni", "NEW"],
        ["TV4", "192.168.50.2", "BRN-IT", "monitorovaci", "UNKNOWN"]
    ]


def convert_B2MB(size: float) -> float:
    return round(size / 2**20, 2)


def get_file_data() -> list[list[str]]:
    # TODO: mount network shared directory and FETCH necessary data
    filestats = []
    p = getcwd()
    # chdir("C:/Users/rtval/Pictures/Camera Roll")
    chdir("/Users/xpatrik/Downloads/images")
    for file in scandir():
        name = file.name.split(".")[0]
        ext = file.name.split(".")[1]
        maintime = datetime.fromtimestamp(file.stat().st_mtime).isoformat(
            timespec='minutes')
        if ext in SUPPORTED_EXT_LIST:
            filestats.append(
                [name, ext, str(convert_B2MB(file.stat().st_size)) + " MiB", maintime])
        # size in bytes, time in seconds
    chdir(p)
    return filestats


def set_tv_data(data: list[list[str]]) -> None:
    # TODO: UPDATE OR INSERT tv data
    pass


def load_from_file() -> list[str]:
    file = filedialog.askopenfilename(title="Vyberte soubor",
                                      filetypes={"text *.txt": "text files"})
    f = open(file, "r")
    line = f.readline()
    if ',' not in line:
        raise ValueError("Chybný formát.")

    list_ = line.split(',')
    f.close()
    # refresh_shown_files(list_=list_)
    return list_


def save_to_file(fileNames) -> None:
    string = ""
    for fileNames in fileNames:
        string += fileNames
        string += ","
    file = filedialog.asksaveasfilename(title="Uložte soubor",
                                        filetypes={"text *.txt": "text files"},
                                        defaultextension=".txt")

    f = open(file, "w+")
    f.write(string)
    f.close()


def clear_dir() -> None:
    for file in scandir(PLAYER_PATH):
        remove(file.path)


def sync_files(file_list: [str]) -> None:
    clear_dir()
    copied = 0
    for file in file_list:
        if exists(SMB_PATH + file):
            copyfile(SMB_PATH + file, PLAYER_PATH + file)
            copied += 1

    if copied == 0:
        copyfile(DEFAULT_IMG_PATH, PLAYER_PATH + "default.png")


def create_link_file(link: str) -> None:
    clear_dir()
    if is_url_valid(link):
        f = open(link, "w+")
        f.write(link)
        f.close()
    else:
        copyfile(DEFAULT_IMG_PATH, PLAYER_PATH + "default.png")
