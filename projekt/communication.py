from enum import Enum
from socket import AF_INET, SOCK_STREAM, socket, SOL_SOCKET, SO_REUSEADDR
from typing import Optional
from urllib.error import URLError

from data_utils import sync_files


class MessageHeader(Enum):
    REPORT_STATE = 0
    SENDING_LIST = 1
    SENDING_LINK = 2

    @staticmethod
    def decode(num: int):
        switcher = {
            MessageHeader.REPORT_STATE.value: MessageHeader.REPORT_STATE,
            MessageHeader.SENDING_LIST.value: MessageHeader.SENDING_LIST,
            MessageHeader.SENDING_LINK.value: MessageHeader.SENDING_LINK,
        }
        if num not in switcher:
            return None
        return switcher[num]


class Message:
    __answer: str
    __header: MessageHeader
    __content: list

    def __init__(self, header: MessageHeader, content: list = None):
        self.__answer = ""
        self.__header = header
        if content is None:
            self.__content = []
        else:
            self.__content = content

    @property
    def answer(self) -> str:
        return self.__answer

    @property
    def header(self) -> MessageHeader:
        return self.__header

    @property
    def content(self) -> list:
        return self.__content

    @content.setter
    def content(self, content: list[str]) -> None:
        self.__content = content

    def build_message(self) -> str:
        if len(self.__content) == 0:
            cont = ""
        else:
            cont = self.__content[0]
            for c in self.__content[1:]:
                cont = f"{cont},{c}"
        return f"{self.__header.value}\n\n{cont}"

    @staticmethod
    def decode(msg: str) -> dict[Optional[str] or list[str]]:
        header = MessageHeader.decode(int(msg.split("\n\n")[0]))
        content = [ServerState.decode(int(msg.split("\n\n")[1]))]
        return {"header": header,
                "content": content}


class ServerState(Enum):
    SUC_COPYING = 0
    SUC_PLAYING = 2
    SUC_NEW = 4
    SUC_DISPLAYING = 6
    ERR_FAULTY = 1
    ERR_NOT_LISTENING = 3
    UNKNOWN = 5

    @staticmethod
    def is_success(state):
        # even means SUCCESS
        return state.value % 2 == 0

    @staticmethod
    def to_str(i) -> Optional[str]:
        l = ["COPYING",
             "FAULTY",
             "prevhrava",
             "DOWN",
             "NEW",
             "UNKNOWN",
             "DISPLAYING"]
        if i < len(l):
            return l[i]
        return None

    @staticmethod
    def decode(num: int):
        switcher = {
            ServerState.SUC_PLAYING.value: ServerState.SUC_PLAYING,
            ServerState.SUC_COPYING.value: ServerState.SUC_COPYING,
            ServerState.SUC_NEW.value: ServerState.SUC_NEW,
            ServerState.ERR_FAULTY.value: ServerState.ERR_FAULTY,
            ServerState.ERR_NOT_LISTENING.value: ServerState.ERR_NOT_LISTENING,
            ServerState.UNKNOWN.value: ServerState.UNKNOWN,
        }
        if num not in switcher:
            return None
        return switcher[num]


class Server:
    __ip_address: str
    __name: str
    __location: Optional[str]
    __cur_state: ServerState
    __type: str

    def __int__(self, ip: str, name: str, location: str = None,
                state: ServerState = ServerState.UNKNOWN):
        self.__name = name
        self.__location = location
        self.__cur_state = state
        self.__ip_address = ip

    @property
    def ip_address(self):
        return self.__ip_address

    @property
    def cur_state(self):
        return self.__cur_state

    @cur_state.setter
    def cur_state(self, state: ServerState):
        self.__cur_state = state

    def to_str(self):
        return f"name: {self.__name}\n" \
               f"state: {self.__cur_state}\n" \
               f"ip: {self.__ip_address}\n" \
               f"loc: {self.__location}"


class Com:
    PORT = 11111

    @staticmethod
    def send_message(msg: Message, recipient: Server):
        message_str = msg.build_message()
        msg_size = 4096

        ip = recipient.ip_address
        server_socket = socket(AF_INET, SOCK_STREAM)
        server_socket.settimeout(10.0)
        print(ip)

        # initial handshake
        try:
            server_socket.connect((ip, Com.PORT))
        except URLError:
            recipient.__setattr__("cur_state", ServerState.ERR_NOT_LISTENING)
            return False
        if len(message_str) > msg_size - len("x\n\n"):
            # TODO code action for message larger then buffer
            return False
        server_socket.send(message_str.encode())
        answer = server_socket.recv(msg_size).decode()
        # TODO define behavior according to answer DONE?
        msg_answer = Message(Message.decode(answer)["header"],
                             content=Message.decode(answer)["content"])

        recipient.__setattr__("cur_state", msg_answer.content[0])
        return True

    @staticmethod
    def reply(s, pid) -> int:
        conn, addr = s.accept()
        with conn:
            data = conn.recv(4096).decode()
            msg = data.split('\n\n')
            if not msg[0].isdecimal() or len(msg) != 2 or MessageHeader.decode(
                    int(msg[0])):
                conn.sendall(f"1\n\n5".encode())
                return 2

            reply_msg: Message
            if int(msg[0]) == MessageHeader.SENDING_LIST.value:
                reply_msg = Message(MessageHeader.SENDING_LIST,
                                    content=[
                                        str(ServerState.SUC_COPYING.value)])
                conn.sendall(reply_msg.build_message().encode())
                msg.pop(0)
                sync_files(msg[0].split(","))

                s.close()
                return MessageHeader.SENDING_LIST.value

            elif int(msg[0]) == MessageHeader.REPORT_STATE.value:
                reply_msg = Message(MessageHeader.REPORT_STATE)
                # TODO fix vlc checker
                reply_msg.__setattr__("content",
                                      [str(ServerState.SUC_PLAYING.value)])
                conn.sendall(reply_msg.build_message().encode())
                return MessageHeader.REPORT_STATE.value
            else:
                reply_msg = Message(MessageHeader.SENDING_LINK)
                reply_msg.__setattr__("content",
                                      [str(ServerState.SUC_DISPLAYING.value)])
                conn.sendall(reply_msg.build_message().encode())
                sync_files(msg[0].split(","))
                return MessageHeader.SENDING_LINK.value

    def server_listen(self, host: str, pid) -> int:
        with socket(AF_INET, SOCK_STREAM) as s:
            s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
            s.bind((host, self.PORT))
            s.listen()
            return Com.reply(s, pid)
