import tkinter as tk
from tkinter import Frame, Label, Entry
from tkinter import ttk
from tkinter.ttk import Combobox

from klient_gui_classes import MyWidget


class Table:
    def __init__(self, header, data, frame: Frame, height: int, font: str):
        self.header = header
        self.data = data
        self.table = ttk.Treeview(frame)
        self.table.pack()
        self.table["columns"] = tuple(header)
        self.table.column("#0", width=0, stretch=tk.NO)
        for coll in header:
            self.table.heading(coll, text=coll, anchor=tk.W)
            self.table.column(coll, width=int(890 / len(header)))
        self.table.configure(height=height)
        self.table.tag_configure("odd", font=(font, 20))
        self.table.tag_configure("even", font=(font, 20), background="#E8E8E8")

    def insert_data(self):
        id_ = 0
        for row in self.data:
            if id_ % 2 == 0:
                tag = "even"
            else:
                tag = "odd"

            self.table.insert(parent='', index='end', iid=str(id_), text='',
                              values=tuple(row), tag=tag)
            id_ += 1

    def get_row_by_name(self, name: str) -> list[str]:
        for row in self.data:
            if row[0] == name:
                return row

    def update(self, data):
        self.delete()
        self.data = data
        self.insert_data()

    def delete(self):
        self.data = []
        for i in self.table.get_children():
            self.table.delete(i)


class Form:
    mw_name: MyWidget
    mw_group: MyWidget
    mw_ip_address: MyWidget
    mw_location: MyWidget
    mw_type: MyWidget
    var_type: tk.StringVar

    def __init__(self, root, font):
        width = 760
        height = 30

        l_atrs = []
        for i in range(4):
            txt = Entry(foreground='black',
                        background="white",
                        font=(font, 20),
                        insertbackground="black", selectborderwidth=1)
            txt.configure(highlightbackground="grey", highlightcolor="black",
                          highlightthickness=3, borderwidth=0)
            l_atrs.append(MyWidget(x=0.2, y=0.3 + i / 10 + 0.05, w_obj=txt,
                                   width=width,
                                   height=height))
        self.mw_name = l_atrs[0]
        self.mw_ip_address = l_atrs[1]
        self.mw_location = l_atrs[2]
        self.mw_group = l_atrs[3]

        l_atrs = []
        field_names = ["Nazev", "IP Adresa", "Umisteni", "Skupina", "Typ"]
        i = 0
        for name in field_names:
            lbl = Label(foreground='black',
                        background="white",
                        font=(font, 20),
                        text=name,
                        anchor=tk.W)
            l_atrs.append(MyWidget(x=0.2, y=0.3 + i / 10, w_obj=lbl,
                                   width=width,
                                   height=height))
            i += 1

        self.var_type = tk.StringVar()
        combo_tk = Combobox(state="readonly", background="white",
                            textvariable=self.var_type, font=(font, 20))
        self.mw_type = MyWidget(x=0.2, y=0.3 + 4 / 10 + 0.05, w_obj=combo_tk,
                                width=width,
                                height=height)
        combo_tk["values"] = ["reklamni", "monitorovaci"]
        self.var_type.set("reklamni")

        self.mw_lbl_name = l_atrs[0]
        self.mw_lbl_ip_address = l_atrs[1]
        self.mw_lbl_location = l_atrs[2]
        self.mw_lbl_type = l_atrs[3]
        self.mw_lbl_group = l_atrs[4]

    def get_atrs(self):
        return [self.mw_name, self.mw_group,
                self.mw_ip_address, self.mw_location,
                self.mw_type] + \
               [self.mw_lbl_name,
                self.mw_lbl_ip_address,
                self.mw_lbl_location,
                self.mw_lbl_type,
                self.mw_lbl_group]

    def fill_yourself(self, name: str, ip_address: str, location: str,
                      type: str,
                      group: str):
        self.mw_name.widget_obj.delete(0, "end")
        self.mw_name.widget_obj.insert(0, name)
        self.mw_ip_address.widget_obj.delete(0, "end")
        self.mw_ip_address.widget_obj.insert(0, ip_address)
        self.mw_group.widget_obj.delete(0, "end")
        self.mw_group.widget_obj.insert(0, group)
        self.var_type.set(type)
        self.mw_location.widget_obj.delete(0, "end")
        self.mw_location.widget_obj.insert(0, location)
