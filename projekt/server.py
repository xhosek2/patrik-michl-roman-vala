import webbrowser
from os import kill, fork
from signal import SIGTERM

from communication import Com, MessageHeader
from data_utils import LINK_FILE
import netifaces as ni
HOST = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']


def main(pid):
    while True:
        c = Com()
        code = c.server_listen(HOST, pid)

        if code == MessageHeader.SENDING_LIST.value:
            kill(pid, SIGTERM)


while True:
    newpid = fork()
    if newpid == 0:
        try:
            f = open(LINK_FILE)
            webbrowser.open_new(f.readline())
            # from selenium import webdriver
            # from selenium.webdriver.chrome.options import Options
            #
            # chrome_options = Options()
            # chrome_options.add_argument("--kiosk")
            #
            # driver = webdriver.Chrome(chrome_options=chrome_options)
            # driver.get(f.readline())
            f.close()
        except IOError:
            exec(open("/home/pi/player.py").read(), globals(), globals())
    elif newpid > 0:
        main(newpid)
    else:
        raise ValueError("initial fork failed")

