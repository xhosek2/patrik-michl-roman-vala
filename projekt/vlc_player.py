from os import chdir, scandir
from time import sleep as wait

from vlc import Media, MediaPlayer
from data_utils import PLAYER_PATH as LOCAL_PATH


def scan_dir() -> [str]:
    chdir(LOCAL_PATH)
    file_list = scandir()
    show_list = []
    for file in file_list:
        show_list.append(file.name)
    return show_list


def play():
    playlist = scan_dir()
    player = MediaPlayer()
    playing = {1, 2, 3, 4}
    while True:
        for pic in playlist:
            print(pic)
            media = Media(pic)
            player.set_media(media)
            player.set_fullscreen(True)
            player.play()
            # wait briefly for it to start
            wait(0.1)

            # while True:
            #     state = player.get_state()
            #     if state not in playing:
            #         break
            while player.get_state() in playing:
                pass


if __name__ == "__main__":
    play()
