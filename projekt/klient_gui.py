import tkinter as tk
from tkinter import Button, Frame, Label, ttk, Entry
from tkinter.ttk import Combobox
from typing import Optional

from PIL import Image, ImageTk

from communication import Com, Message, Server, ServerState, MessageHeader
from data_utils import get_file_data, get_tv_data, load_from_file, save_to_file
from klient_gui_classes import Info, MyWidget, Page
from table_form_classes import Form, Table
from validators import url as is_url_valid

APP_FONT = "TerminessTTF Nerd Font"
URL_FONT = "Courier New"
SERVER_DATA = get_tv_data()
FILE_DATA = get_file_data()


def place_label_info(type_: str, msg: str) -> None:
    if lbl_info.is_active():
        lbl_info.deactivate()
    lbl_info.show(type_, msg)


def move_window(event) -> None:
    # konstanty souvisí z velikosti tlačítka
    window.geometry(f'+{event.x_root - btn_move.winfo_x() - 35}+'
                    f'{event.y_root - btn_move.winfo_y() - 35}')


def fill_tv_data_in(_) -> None:
    if PAGES["EDIT"].is_active:
        tv = tbl_servers.get_row_by_name(selected_tv.get())
        form.fill_yourself(tv[0], tv[1], tv[2], tv[3], tv[4])
        # TODO: grupa


def close() -> None:
    window.destroy()


def load() -> None:
    try:
        i = 0
        rows_to_select = []
        rows_to_be_selected = load_from_file()
        for row in tbl_cont.data:
            if row[0] in rows_to_be_selected:
                rows_to_select.append(str(i))
            i += 1
        tbl_cont.table.selection_set(tuple(rows_to_select))
        place_label_info("success", "Soubor načten.")
    except ValueError as ve:
        place_label_info("failure", "Prázdný soubor.")
    except IOError as e:
        place_label_info("failure", "Nevybrán soubor.")
    except:
        place_label_info("failure", "Nelze otevřít.")


def save() -> None:
    try:
        i = 0
        selected = []
        for row in tbl_cont.data:
            if str(i) in tbl_cont.table.selection():
                selected.append(row[0])
            i += 1
        save_to_file(selected)
        place_label_info("success", "Soubor uložen.")
    except IOError as e:
        place_label_info("failure", "Nevybrán soubor.")
    except:
        place_label_info("failure", "Nelze otevřít.")


def send() -> None:
    sending_playlist = True
    srv = None
    for srv_data in tbl_servers.data:
        if srv_data[0] == selected_tv.get():
            srv = Server()
            srv.__int__(name=srv_data[0], ip=srv_data[1], location=srv_data[2])
            if srv_data[3] == "monitorovaci":
                sending_playlist = False

    if srv is None:
        place_label_info("FAILURE", "Server neznámý.")
        return

    if sending_playlist:
        msg = build_msg_playlist()
    else:
        msg = build_msg_link()
    if msg is None:
        return

    try:
        Com.send_message(msg, srv)
    except:
        srv.__setattr__("cur_state", ServerState.ERR_NOT_LISTENING)
    if srv.cur_state.is_success(srv.cur_state):
        place_label_info("SUCCESS", "Zpráva odeslána.")
    else:
        place_label_info("FAILURE", "Server nedostupný.")


def build_msg_playlist() -> Message:
    pl = []
    for i in tbl_cont.table.selection():
        pl.append(tbl_cont.data[int(i)][0])

    return Message(MessageHeader.SENDING_LIST, pl)


def build_msg_link() -> Optional[Message]:
    link = txt_tk_url.get()
    if is_url_valid(link):
        return Message(MessageHeader.SENDING_LIST, [link])
    place_label_info("FAILURE", "URL nevalidní.")
    return None


def refresh() -> None:
    if PAGES["SEND"].is_active:
        tbl_cont.update(get_file_data())
    elif PAGES["STATUS"].is_active:
        pass


def ask_for_state() -> None:
    for srv_data in tbl_servers.data:
        srv = Server()
        srv.__int__(name=srv_data[0], ip=srv_data[1],
                    location=srv_data[2])
        place_label_info("SUCCESS", f"Dotazována {srv_data[0]}.")
        try:
            Com.send_message(Message(MessageHeader.REPORT_STATE, []), srv)
        except:
            srv.__setattr__("cur_state", ServerState.ERR_NOT_LISTENING)
        if srv_data[4] != ServerState.to_str(
                ServerState.SUC_NEW.value) or ServerState.is_success(
            srv.cur_state):
            srv_data[4] = ServerState.to_str(srv.cur_state.value)
        tbl_servers.update(tbl_servers.data)
    place_label_info("SUCCESS", f"Data získána.")


def minimize() -> None:
    window.overrideredirect(False)
    window.iconify()
    window.overrideredirect(True)


def change_page(btn_name: str) -> None:
    for page_name, page_obj in PAGES.items():
        if page_obj.is_active:
            page_obj.unplace_widgets()
    PAGES[btn_name].place_widgets()
    if lbl_info.is_active():
        lbl_info.deactivate()


window = tk.Tk()

# BEGIN button images bg
img_send_p = ImageTk.PhotoImage(Image.open("ico/send_p.png"))
img_minimize = ImageTk.PhotoImage(Image.open("ico/minimize.png"))
img_close = ImageTk.PhotoImage(Image.open("ico/close.png"))
img_move = ImageTk.PhotoImage(Image.open("ico/move.png"))
img_add = ImageTk.PhotoImage(Image.open("ico/add.png"))
img_edit = ImageTk.PhotoImage(Image.open("ico/edit.png"))
img_status = ImageTk.PhotoImage(Image.open("ico/status2.png"))
img_about = ImageTk.PhotoImage(Image.open("ico/info.png"))
img_send_p_clicked = ImageTk.PhotoImage(Image.open("ico/send_p-b.png"))
img_add_clicked = ImageTk.PhotoImage(Image.open("ico/add-b.png"))
img_edit_clicked = ImageTk.PhotoImage(Image.open("ico/edit-b.png"))
img_status_clicked = ImageTk.PhotoImage(Image.open("ico/status2-b.png"))
img_about_clicked = ImageTk.PhotoImage(Image.open("ico/info-b.png"))
img_logo = ImageTk.PhotoImage(Image.open("ico/logo.png"))
img_save = ImageTk.PhotoImage(Image.open("ico/save.png"))
img_load = ImageTk.PhotoImage(Image.open("ico/load.png"))
img_send = ImageTk.PhotoImage(Image.open("ico/send.png"))
img_stats = ImageTk.PhotoImage(Image.open("ico/stats.png"))
img_new = ImageTk.PhotoImage(Image.open("ico/new.png"))
img_delete = ImageTk.PhotoImage(Image.open("ico/delete.png"))
img_refresh = ImageTk.PhotoImage(Image.open("ico/refresh.png"))
img_edit_b = ImageTk.PhotoImage(Image.open("ico/edit-tv.png"))
img_reject = ImageTk.PhotoImage(Image.open("ico/reject.png"))
img_apply = ImageTk.PhotoImage(Image.open("ico/apply.png"))
# END button images bg

# BEGIN buttons changing pages
btn_send_page = tk.Button(window, image=img_send_p, borderwidth=0,
                          command=lambda: change_page("SEND"))
btn_send_page.place(relx=0.01, rely=0.18,
                    height=img_send_p.height(), width=img_send_p.width())

btn_add_page = tk.Button(window, image=img_add, borderwidth=0,
                         command=lambda: change_page("ADD"))
btn_add_page.place(relx=0.01, rely=0.33,
                   height=img_add.height(), width=img_add.width())

btn_edit_page = tk.Button(window, image=img_edit, borderwidth=0,
                          command=lambda: change_page("EDIT"))
btn_edit_page.place(relx=0.01, rely=0.48, height=img_edit.height(),
                    width=img_edit.width())

btn_status_page = tk.Button(window, image=img_status, borderwidth=0,
                            command=lambda: change_page("STATUS"))
btn_status_page.place(relx=0.01, rely=0.63, height=img_status.height(),
                      width=img_status.width())

btn_about_page = tk.Button(window, image=img_about_clicked, borderwidth=0,
                           command=lambda: change_page("ABOUT"))
btn_about_page.place(relx=0.01, rely=0.85, height=img_about_clicked.height(),
                     width=img_about_clicked.width())

lbl_logo = tk.Label(window, image=img_logo, borderwidth=0)
lbl_logo.place(relx=0.01, rely=0.005, height=img_logo.height(),
               width=img_logo.width())
# END buttons changing pages

# BEGIN buttons controlling windows
btn_minimize = tk.Button(window, image=img_minimize, borderwidth=0,
                         command=minimize)
btn_minimize.place(relx=0.780, rely=0.01, height=img_minimize.height(),
                   width=img_minimize.width())

btn_move = tk.Button(window, image=img_move, borderwidth=0)
btn_move.place(relx=0.855, rely=0.01, height=img_move.height(),
               width=img_move.width())
btn_move.bind('<B1-Motion>', move_window)

btn_close = tk.Button(window, image=img_close, borderwidth=0, command=close)
btn_close.place(relx=0.93, rely=0.01, height=img_close.height(),
                width=img_close.width())
# END buttons controlling windows

# BEGIN buttons controlling pages
button_save = Button(window, image=img_save, borderwidth=0, command=save)
button_load = Button(window, image=img_load, borderwidth=0, command=load)
button_send = Button(window, image=img_send, borderwidth=0, command=send)
button_stats = Button(window, image=img_stats, borderwidth=0,
                      command=ask_for_state)
button_new = Button(window, image=img_new, borderwidth=0)
button_delete = Button(window, image=img_delete, borderwidth=0)
button_refresh = Button(window, image=img_refresh, borderwidth=0,
                        command=refresh)
button_edit = Button(window, image=img_edit_b, borderwidth=0)

btn_save = MyWidget(x=0.225, y=0.18, w_obj=button_save,
                    height=img_send.height(), width=img_send.width())
btn_load = MyWidget(x=0.30, y=0.18, w_obj=button_load,
                    height=img_send.height(), width=img_send.width())
btn_send = MyWidget(x=0.375, y=0.18, w_obj=button_send,
                    height=img_send.height(), width=img_send.width())
btn_stats = MyWidget(x=0.225, y=0.18, w_obj=button_stats,
                     height=img_send.height(), width=img_send.width())
btn_new = MyWidget(x=0.15, y=0.18, w_obj=button_new,
                   height=img_send.height(), width=img_send.width())
btn_delete = MyWidget(x=0.15, y=0.18, w_obj=button_delete,
                      height=img_send.height(), width=img_send.width())
btn_refresh = MyWidget(x=0.15, y=0.18, w_obj=button_refresh,
                       height=img_send.height(), width=img_send.width())
btn_edit = MyWidget(x=0.225, y=0.18, w_obj=button_edit,
                    height=img_send.height(), width=img_send.width())
# END buttons controlling pages

# BEGIN buttons controlling windows
btn_minimize = tk.Button(window, image=img_minimize, borderwidth=0,
                         command=minimize)
btn_minimize.place(relx=0.780, rely=0.01, height=img_minimize.height(),
                   width=img_minimize.width())

btn_move = tk.Button(window, image=img_move, borderwidth=0)
btn_move.place(relx=0.855, rely=0.01, height=img_move.height(),
               width=img_move.width())
btn_move.bind('<B1-Motion>', move_window)

btn_close = tk.Button(window, image=img_close, borderwidth=0, command=close)
btn_close.place(relx=0.93, rely=0.01, height=img_close.height(),
                width=img_close.width())
# END buttons controlling windows

# BEGIN lines for better visual separation
tk.ttk.Separator(window, orient=tk.VERTICAL).place(relx=0.13, relheight=1)
tk.ttk.Separator(window, orient=tk.HORIZONTAL).place(rely=0.165, relwidth=0.13)
# END lines for better visual separation

# BEGIN label for app title
lbl_title = tk.Label(window, text="MoonPy APP", bg="white", fg="black",
                     font=(APP_FONT, 35))
lbl_title.place(relx=0.15, rely=0.03, relheight=0.1)

text_about = "Autoři: Patrik Michl <xmichl@mendelu.cz>\n" \
             "        Roman Vala <xvala6@mendelu.cz>\n" \
             "Verze: x.xxxx\n" \
             "Datum poslední aktualizace: teď někdy\n" \
             "Účel: získat spoustu bodů\n" \
             "Funkce:"

label_about = Label(window, text=text_about, bg="white",
                    font=(APP_FONT, 20), justify=tk.LEFT, fg="black")

lbl_about = MyWidget(x=0.15, y=0.3, height=200, width=800,
                     w_obj=label_about)

style = ttk.Style(window)
# set ttk theme to "clam" which support the fieldbackground option
style.theme_use("clam")
#style.configure("Treeview", background="white",
style.configure("Treeview", background="white",rowheight=30,
                fieldbackground="white", foreground="black")
style.configure("Frame", background="white",
                fieldbackground="white", foreground="white")
style.configure('Treeview.Heading', background="white", font=(APP_FONT, 20))
style.map("Treeview", background=[("selected", "#007dc5")])
style.configure('Entry', highlightbackground="grey", highlightcolor="black",
                highlightthickness=3, borderwidth=0, font=(APP_FONT, 20))

table_tk_cont_frame = Frame(window)
table_tk_cont_frame.configure(background='white')
tbl_cont = Table(["nazev", "format", "velikost", "datum"],
                 FILE_DATA, table_tk_cont_frame,
                 400,
                 font=APP_FONT)
tbl_cont.insert_data()
tbl_cont_frame = MyWidget(0.15, 0.3, table_tk_cont_frame, 350, 900)
table_tk_stat_frame = Frame(window)
table_tk_stat_frame.configure(background='white')
tbl_servers = Table(["nazev", "ip adresa", "umisteni", "typ", "status"],
                    SERVER_DATA,
                    table_tk_stat_frame,
                    400, font=APP_FONT)
tbl_servers.insert_data()
tbl_tvs_frame = MyWidget(0.15, 0.3, table_tk_stat_frame, 500, 900)
txt_tk_url = Entry(window,
                   width=100, foreground='black',
                   background="white", font=(URL_FONT, 20))
txt_tk_url.configure(highlightbackground="grey", highlightcolor="black",
                     highlightthickness=3, borderwidth=0)
# txt_tk_url.pack(anchor=)
txt_url = MyWidget(0.155, 0.9, txt_tk_url, 40, 800)
lbl_tk_url = Label(window, width=100, height=40, text="URL",
                   font=(APP_FONT, 35), bg="white", foreground="black")
lbL_url = MyWidget(0.155, 0.82, lbl_tk_url, 40, 80)

selected_tv = tk.StringVar()
combo_tk = Combobox(state="readonly", background="white",
                    textvariable=selected_tv, font=(APP_FONT, 35))
combo_tk["values"] = [row[0] for row in SERVER_DATA]
if len(combo_tk["values"]) != 0:
    combo_tk.set(combo_tk["values"][0])
# window.option_add('*TCombobox*Listbox.font', APP_FONT)
combo = MyWidget(x=0.6, y=0.19, w_obj=combo_tk, width=400, height=50)
combo_tk.option_add("*TCombobox*Listbox.background", "white")
combo_tk.option_add("*TCombobox*Listbox.foreground", "black")
combo_tk.option_add("*TCombobox*Listbox.font", (APP_FONT, 35))

style.map('TCombobox', fieldbackground=[('readonly', 'white')])
style.map('TCombobox', selectbackground=[('readonly', 'white')])
style.map('TCombobox', selectforeground=[('readonly', 'black')])

combo_tk.bind('<<ComboboxSelected>>', fill_tv_data_in)

form = Form(window, APP_FONT)

lbl_info = Info(window, APP_FONT, img_apply, img_reject)

# BEGIN lists of page elements
SEND_PAGE = [btn_refresh, btn_save, btn_load, btn_send, tbl_cont_frame, txt_url,
             lbL_url,
             combo]
ADD_PAGE = [btn_new] + form.get_atrs()
EDIT_PAGE = [btn_edit, btn_delete, combo] + form.get_atrs()
STATUS_PAGE = [btn_refresh, btn_stats, tbl_tvs_frame]
ABOUT_PAGE = [lbl_about]
# END lists of page elements

page_about = Page(ABOUT_PAGE, img_about_clicked, img_about, btn_about_page)
page_about.place_widgets()

PAGES = {
    "SEND": Page(SEND_PAGE, img_send_p_clicked, img_send_p, btn_send_page),
    "ADD": Page(ADD_PAGE, img_add_clicked, img_add, btn_add_page),
    "EDIT": Page(EDIT_PAGE, img_edit_clicked, img_edit, btn_edit_page),
    "STATUS": Page(STATUS_PAGE, img_status_clicked, img_status,
                   btn_status_page),
    "ABOUT": page_about
}

window.geometry("1080x750")
window.minsize(width=1080, height=750)
window.configure(bg="white")

window.overrideredirect(True)
# window.attributes("-type", "dock")
window.title("MoonPy APP")
window.focus_force()
window.mainloop()
