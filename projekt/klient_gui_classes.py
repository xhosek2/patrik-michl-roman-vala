import tkinter as tk
from tkinter import Button, Label, Widget


class MyWidget:
    x: float
    y: float
    height: float
    width: float
    widget_obj: Widget

    def __init__(self, x: float, y: float, w_obj: Widget,
                 height: float, width: float):
        self.x = x
        self.y = y
        self.height = height
        self.width = width
        self.widget_obj = w_obj

    def place(self):
        self.widget_obj.place(relx=self.x, rely=self.y,
                              height=self.height, width=self.width)

    def unplace(self):
        self.widget_obj.place_forget()


class Page:
    widgets: [MyWidget]
    image_clicked: tk.PhotoImage
    image: tk.PhotoImage
    is_active: bool
    button: Button

    def __init__(self, widgets: [MyWidget], image_clicked: tk.PhotoImage,
                 image: tk.PhotoImage, button: Button):
        self.widgets = widgets
        self.is_active = False
        self.image_clicked = image_clicked
        self.image = image
        self.button = button

    def activate(self):
        self.is_active = True
        self.button.configure(image=self.image_clicked)

    def deactivate(self):
        self.is_active = False
        self.button.configure(image=self.image)

    def place_widgets(self):
        for widget in self.widgets:
            widget.place()
        self.activate()

    def unplace_widgets(self):
        for widget in self.widgets:
            widget.unplace()
        self.deactivate()

    @property
    def is_activated(self) -> bool:
        return self.is_active


class Info:
    GREEN = "#93C590"
    YELLOW = "#FFD757"
    RED = "#FF6057"
    COLORS = {"WARNING": YELLOW, "SUCCESS": GREEN, "FAILURE": RED}
    lbl: Label
    btn_reject: Button
    btn_apply: Button
    size: (float, float)

    def __init__(self, root, font, apply, reject):
        self.lbl = Label(root, bg="white",
                         font=(font, 20), justify=tk.LEFT, fg="black")
        #self.lbl.place(relx=0.35, rely=0.03, relwidth=0.4, relheight=0.1)
        self.lbl.place(relx=0.38, rely=0.03, relwidth=0.33, relheight=0.1)
        self.btn_apply = Button(root, image=apply, borderwidth=0)
        self.btn_reject = Button(root, image=reject, borderwidth=0)
        self.size = (apply.width(), apply.height())

    def show(self, type: str, msg: str) -> None:
        text = f"{type.upper()}: {msg}"
        self.lbl.configure(text=text)
        self.lbl.configure(bg=self.COLORS[type.upper()])
        if self.is_warning():
            self.btn_apply.place(relx=0.69, rely=0.065,
                                 width=self.size[0],
                                 height=self.size[1])
            self.btn_reject.place(relx=0.72, rely=0.065,
                                  width=self.size[0],
                                  height=self.size[1])

    def is_active(self) -> bool:
        return self.lbl.cget("bg") != "white"

    def deactivate(self) -> None:
        if self.is_warning():
            self.btn_apply.place_forget()
            self.btn_reject.place_forget()
        self.lbl.configure(bg="white", text="")

    def is_warning(self) -> bool:
        return self.lbl.cget("bg") == self.YELLOW
